module gitlab.com/hmajid2301/zellij-session-manager-plugin

go 1.21

require (
	gitlab.com/scabala/zelligo v0.0.0-20240119184615-947a7a8a10d5 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
)
